# Bifurcación de un repositorio
Bifurca (haz un fork) del repositorio
El repositorio resultante debe quedar como un repositorio de tu usuario.
Una vez que tengas ese nuevo repositorio, utilizando la interfaz web de GitLab, modifica el fichero README.md que hay en él para añadir, después del enunciado, el texto "Este es el repositorio de ", donde  es tu nombre.
Asegúrate de que el repositorio es público (visible para todos los visitantes) o interno (visible para los usuarios que se hayan autenticado.
NURIA MORDILLO 